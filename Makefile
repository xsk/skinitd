build-release:
	cargo build --release

build-release-compressed: build-release
	upx -q --best --lzma target/release/skinitd

build-docker: build-release
	docker build . -t chrskev/skinitd:$(TAG) --no-cache

run-docker:
	docker run chrskev/skinitd:$(TAG)

push-docker:
	docker push chrskev/skinitd:$(TAG)

build-release-push-docker: build-release build-docker push-docker

.PHONY: all clean test