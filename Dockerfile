FROM debian:sid-slim

COPY ./target/release/skinitd /skinitd
RUN chmod +x /skinitd

ENTRYPOINT ["/skinitd"]
