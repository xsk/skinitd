extern crate log;
extern crate nix;
extern crate signal_hook;

use env_logger::Env;
use log::{error, info, warn};
use nix::sys::signal::{self, Signal};
use nix::unistd::{execv, fork, getpid, getppid, ForkResult};
use std::ffi::{CStr, CString};
use std::process::exit;
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::Arc;
use std::thread::sleep;
use std::{env, time};

// Pausing for some amount of time before checking for signal updates
const WAIT_DURATION: time::Duration = time::Duration::from_millis(500);
const CHILD_WAIT_DURATION: time::Duration = time::Duration::from_millis(2000);

fn main() {
    // Initialize env logger for logging
    let env = Env::default()
        .filter_or("LOG_LEVEL", "trace")
        .write_style_or("LOG_STYLE", "always");

    env_logger::init_from_env(env);

    // Atomic booleans will now be pointed to the same memory location
    // We are registering these here, so when signal hook changes them, we will know
    let sig_chld = Arc::new(AtomicBool::new(false));
    let sig_term = Arc::new(AtomicBool::new(false));
    let sig_inte = Arc::new(AtomicBool::new(false));
    let sig_quit = Arc::new(AtomicBool::new(false));

    signal_hook::flag::register(signal_hook::consts::signal::SIGCHLD, Arc::clone(&sig_chld))
        .unwrap();
    signal_hook::flag::register(signal_hook::consts::signal::SIGTERM, Arc::clone(&sig_term))
        .unwrap();
    signal_hook::flag::register(signal_hook::consts::signal::SIGINT, Arc::clone(&sig_inte))
        .unwrap();
    signal_hook::flag::register(signal_hook::consts::signal::SIGQUIT, Arc::clone(&sig_quit))
        .unwrap();

    // Get process id for later checks
    let my_pid: i32 = getpid().as_raw();
    info!("skinitd process id {}", my_pid);

    // Check if skinitd is running as init process
    if my_pid != 1 {
        //TODO: Specify how much of a problem this is for us
        //exit(0x1);
        warn!("Not init, Will still spawn the process provided.")
    }

    // Get the starting argument(s). Expecting the first argument to be the process we need to spawn.
    // args[0] is self
    let args: Vec<String> = env::args().collect();
    if args.len() < 2 {
        error!("No arguments provided. Please supply a starting process.");
        exit(0x1);
    }

    // Remove ourselves from the argument list.
    // While Getting the process name we will use
    // let process_names: Vec<String> = args.drain(0..2).collect();
    //
    // Process path should be absolute
    // let process_path: String = process_names[1].to_string();

    let child_pid = match unsafe { fork() } {
        Ok(ForkResult::Child) => {
            info!("[child] PID is {} and PPID is {}.", getpid(), getppid());

            let args_cstring: Vec<CString> = args
                .iter()
                .map(|s| CString::new(s.as_bytes()).unwrap())
                .collect();

            let args_cstr: Vec<&CStr> = args_cstring.iter().map(|c| c.as_c_str()).collect();

            info!(
                "Running: {:?} with arguments: {:?}",
                &args_cstr[1],
                &args_cstr[2..]
            );

            info!("[child] Starting process.");

            // Following execv, we need to provide the argument list, starting with the current process path first.
            // Slice of args is correct, as the process name is handled in execv
            // TODO: Handle errors here
            match execv(&args_cstr[1], &args_cstr[1..]) {
                Ok(_) => unreachable!("execv() ok()!"),
                Err(err) => unreachable!("execv() err(): {}", err),
            }
        }

        Ok(ForkResult::Parent { child }) => {
            info!("fork() child PID: {}.", child);
            child
        }

        Err(err) => {
            panic!("fork() error: {}", err);
        }
    };

    info!("Starting Skinitd loop");

    loop {
        // TODO:  Forward signal to child process
        // TODO:  Adopt orphan processes
        // TODO:  Reap Zombie processes

        if sig_chld.load(Ordering::Relaxed) {
            info!(
                "Got a SIGCHLD signal from child process {} !.. exiting...",
                child_pid
            );
            break;
        }
        if sig_term.load(Ordering::Relaxed) {
            info!("Got a SIGTERM signal !.. sending to child process and exiting...");
            // Forward SIGTERM to child process.
            match signal::kill(child_pid, Signal::SIGTERM) {
                Ok(_) => {
                    info!(
                        "Signal sent successfully, waiting {:?} for child to exit",
                        CHILD_WAIT_DURATION
                    );
                    // TODO: Make Check to see if child process ended in time
                    sleep(CHILD_WAIT_DURATION);
                }
                Err(err) => info!("Signal error: {}", err),
            }

            break;
        }
        if sig_inte.load(Ordering::Relaxed) {
            info!("Got a SIGINT signal !.. exiting...");
            break;
        }
        if sig_quit.load(Ordering::Relaxed) {
            info!("Got a SIGQUIT signal !.. exiting...");
            break;
        }

        sleep(WAIT_DURATION);
    }

    info!(
        "SIGCHLD: {:?}, SIGINT: {:?}, SIGTERM: {:?}, SIGQUIT: {:?}",
        sig_chld, sig_inte, sig_term, sig_quit
    );
}
